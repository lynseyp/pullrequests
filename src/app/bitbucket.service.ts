import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpParams } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';

const configUrl = 'assets/config.json';

@Injectable({
  providedIn: 'root'
})
export class BitbucketService {

  constructor(private http: HttpClient) { }

  getPullReqs(repositoryName: string): Observable<any> {
    console.log('getPullReqs Start: ' + new Date());
    const url = 'https://api.bitbucket.org/2.0/repositories/landmarkinfo/' + repositoryName +  '/pullrequests?status=OPEN';
    const auth = btoa('lynseyp' + ':' + 'hPYm3Fy7CzePNgKGfukM');
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'authorization': 'Basic ' + auth,
      })
    };

    return this.http.get(url, httpOptions);
  }

  getRepositoriesForProj(project: string): Observable<any> {
    console.log('getRepositoriesForProj');
    project = '%3D%22' + project + '%22';
    const url = 'https://api.bitbucket.org/2.0/repositories/landmarkinfo/?q=project.key' + project ;
    const auth = btoa('lynseyp' + ':' + 'hPYm3Fy7CzePNgKGfukM');
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'authorization': 'Basic ' + auth,
      })
    };

    return this.http.get(url, httpOptions);
  }

  async getRepositoriesForProject(project: string) {
    let allRepos: any;
    console.log('getRepositoriesForProject');
    allRepos = await this.getRepositoriesForProj(project).toPromise();
    // only return the repos that have been updated in the last 2 weeks
    let returnRepos = this.getActiveRepositories(allRepos.values);
    return returnRepos;
  }

  async getPullRequests(project: string) {
    let repos: any;
    console.log('getPullRequests');
    repos = await this.getPullReqs(project).toPromise();
    console.log('getPullReqs Finish: ' + new Date());
    return repos;
  }
  getClassName(prs: Array<any>): string[] {
    let oldest = new Date();

    let className = 'well';
    let badgeName = 'default';
    const minutes_ago_60 = new Date(+new Date - (1000 * 60 * 60 * 1));


    for (const pr of prs) {
      if ( oldest > new Date(pr.created_on) ) {
        oldest = new Date(pr.created_on);
      }
    }

    if (prs.length > 0 ) {
      if (oldest < minutes_ago_60 ) {
        className = 'alert alert-danger';
        badgeName = 'danger';
      } else {
        className = 'alert alert-success';
        badgeName = 'success';
      }
    }
    return [className, badgeName];
  }
  getActiveRepositories(repos: Array<any>) {

    const days_ago_14 = new Date(+new Date - 1000 * 60 * 60 * 24 * 14) ;

    console.log('getActiveRepositories');
    let returnRepos = new Array<any>();

    for (const repo of repos) {
      if ( new Date(repo.updated_on) > days_ago_14 ) {
        returnRepos.push(repo);
      }
    }

    return returnRepos;
  }
}

export class Repository {
  name: string;
  count: any;
  displayName: string;
  className: string;
  badgeName: string;
}
