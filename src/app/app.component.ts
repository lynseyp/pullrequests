import { Component } from '@angular/core';
import { BitbucketService } from './bitbucket.service';
import { Observable } from 'rxjs/Observable';
import { Repository } from './bitbucket.service';
import { environment } from '../environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(private bitbucket: BitbucketService ) { }

  title = 'Open Pull Requests';
  projectName = environment.projectName;
  projectKey = environment.projectKey;
  repositoryList = this.getRepositories();


  async getRepositories() {
    // tslint:disable-next-line:prefer-const
    let repoList = new Array<Repository>();
    let repos: any;

    repos = await this.bitbucket.getRepositoriesForProject(this.projectKey);

    for (const repo of repos) {
      const repo1 = new Repository;
      let pr: any;


      repo1.name = repo.slug;
      repo1.displayName = repo.name;
      pr = await this.bitbucket.getPullRequests(repo.slug);
      const names = await this.bitbucket.getClassName(pr.values);
      repo1.count = pr.size;
      repo1.className = names[0];
      repo1.badgeName = names[1];

      repoList.push(repo1);
    }
      return repoList;
    }

  }



